package main

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"word-search-in-files/pkg/searcher"
)

func main() {

	myfs := searcher.NewFileSystem()

	search := searcher.New(myfs, "./examples")

	mux := http.NewServeMux()

	mux.HandleFunc("GET /files/search/", func(w http.ResponseWriter, r *http.Request) {
		word := r.FormValue("word")

		fff, err := search.Search(word)
		if err != nil {
			if errors.Is(err, searcher.ErrEmptyWord) {
				http.Error(w, "EMPTY_PARAMETER", http.StatusBadRequest)
				return
			}
			http.Error(w, "SERVER_ERROR", http.StatusInternalServerError)
			return
		}

		if len(fff) == 0 {
			w.WriteHeader(http.StatusOK)
			answer := fmt.Sprintf("word \"%s\" not found in files", word)
			w.Write([]byte(answer))
		}

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(strings.Join(fff, "\n")))
	})

	http.ListenAndServe("localhost:24680", mux)

}
