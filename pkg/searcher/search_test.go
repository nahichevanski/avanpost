package searcher

import (
	"io/fs"
	"reflect"
	"sort"
	"testing"
	"testing/fstest"
)

func TestSearcher_Search(t *testing.T) {
	type fields struct {
		FS fs.FS
	}
	type args struct {
		word string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantFiles []string
		wantErr   bool
	}{
		{
			name: "Ok",
			fields: fields{
				FS: fstest.MapFS{
					"file1.txt": {Data: []byte("World")},
					"file2.txt": {Data: []byte("World1")},
					"file3.txt": {Data: []byte("Hello World")},
				},
			},
			args:      args{word: "World"},
			wantFiles: []string{"file1", "file3"},
			wantErr:   false,
		},
		{
			name: "No matches",
			fields: fields{
				FS: fstest.MapFS{
					"file1.txt": {Data: []byte("World")},
					"file2.txt": {Data: []byte("World1")},
					"file3.txt": {Data: []byte("Hello World")},
				},
			},
			args:      args{word: "Hell"},
			wantFiles: nil,
			wantErr:   false,
		},
		{
			name: "Empty Word",
			fields: fields{
				FS: fstest.MapFS{
					"file1.txt": {Data: []byte("World")},
					"file2.txt": {Data: []byte("World1")},
					"file3.txt": {Data: []byte("Hello World")},
				},
			},
			args:      args{word: ""},
			wantFiles: nil,
			wantErr:   true,
		},
		{
			name: "Error",
			fields: fields{
				FS: fstest.MapFS{
					"file1txt": {Data: nil},
					"file2txt": {Data: []byte("World1")},
					"file3txt": {Data: []byte("Hello World")},
				},
			},
			args:      args{word: "World"},
			wantFiles: nil,
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Searcher{
				FS: tt.fields.FS,
			}
			gotFiles, err := s.Search(tt.args.word)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// if !reflect.DeepEqual(gotFiles, tt.wantFiles) {
			// 	t.Errorf("Search() gotFiles = %v, want %v", gotFiles, tt.wantFiles)
			// }
			if !reflect.DeepEqual(normalizeStringSlice(gotFiles), normalizeStringSlice(tt.wantFiles)) {
				t.Errorf("Search() gotFiles = %v, want %v", gotFiles, tt.wantFiles)
			}
		})
	}
}

// при конкурентоном выполнении метода Search элементы добавляются в неопределенном порядке,
// поэтому чтобы тесты прошли я был вынужден отсортировать оба среза
func normalizeStringSlice(s []string) []string {

	normalized := make([]string, len(s))
	copy(normalized, s)

	sort.Strings(normalized)

	return normalized
}
