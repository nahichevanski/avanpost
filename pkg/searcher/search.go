package searcher

import (
	"errors"
	"io/fs"
	"os"
	"runtime"
	"strings"
	"sync"
	"word-search-in-files/pkg/internal/dir"

	"golang.org/x/sync/errgroup"
)

type MyFS struct{}

func (m *MyFS) Open(name string) (fs.File, error) {
	return os.Open(name)
}

func NewFileSystem() *MyFS {
	return &MyFS{}
}

type Searcher struct {
	FS        fs.FS
	SearchDir string
}

func New(fs fs.FS, searchDir string) *Searcher {
	return &Searcher{
		FS:        fs,
		SearchDir: searchDir,
	}
}

var ErrEmptyWord = errors.New("empty word")

func (s *Searcher) Search(word string) ([]string, error) {

	word = strings.TrimSpace(word)
	if word == "" {
		return nil, ErrEmptyWord
	}

	files, err := dir.FilesFS(s.FS, s.SearchDir)
	if err != nil {
		return nil, err
	}

	var fileNames []string

	var g errgroup.Group

	mu := sync.Mutex{}
	size := runtime.NumCPU()
	pool := make(chan struct{}, size)

	for i := 0; i < size; i++ {
		pool <- struct{}{}
	}

	for _, file := range files {

		<-pool

		g.Go(func() error {

			pool <- struct{}{}

			// if file == "examples/file1.txt" {
			// 	return errors.New("test error")
			// }

			f, err := s.FS.Open(file)
			if err != nil {
				return err
			}
			defer f.Close()

			stat, err := f.Stat()
			if err != nil {
				return err
			}

			data := make([]byte, stat.Size())
			_, err = f.Read(data)
			if err != nil {
				return err
			}

			name := strings.SplitN(stat.Name(), ".", 2)

			if checkWordInText(string(data), word) {
				mu.Lock()
				fileNames = append(fileNames, string(name[0]))
				mu.Unlock()
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	return fileNames, nil
}

func checkWordInText(fullString string, substring string) bool {
	words := strings.Fields(fullString)
	for _, word := range words {
		if word == substring {
			return true
		}
	}
	return false
}
